# Project Style Guides

Every major open-source project has its own style guide: a set of conventions (sometimes arbitrary) about how to write code for that project. It is much easier to understand a large codebase when all the code in it is in a consistent style.


“Style” covers a lot of ground, from “use camelCase for variable names” to “never use global variables” to “never use exceptions.” This coding style is from google open source project. How google maintain there open source project and follow the style.


### Style Guides

* [Python Style Guide][py]

* [HTML/CSS Style Guide][htmlcss]

* [JavaScript Style Guide][js]

* [PHP Style Guide][php]

* [Java StyleGuide][java]

* [C# Style Guide][csharp]

* [C++ Style Guide][cpp]

* [Shell Style Guide][sh]

* [Vimscript Style Guide][vim] 


### IDE (Integrated Development Environment)
IDE you may use for the following language.

* Python ([PyCharm][pycharm])
* PHP ([PhpStorm][phpstorm])
* HTML/CSS/JavaScript ([WebStorm][webstorm])
* Java ([Intellij Idea][idea])
* C++/C# ([Microsoft Visual Studio 2015] [visualstudio])

### Hackable Text Editor (Advanced Text Editor)
Text Editor for doing IDE lavel work and Scripting.

* [Atom][atom]
* [Visual Studio Code][vcode]
* [Sublime Text] [sublime]


###### Reference
* [Google Style Guides][googleguide]
* [Popular Coding Convention on Github][convention]


[googleguide]: https://github.com/google/styleguide
[convention]:http://sideeffect.kr/popularconvention
[cpp]: https://google.github.io/styleguide/cppguide.html
[java]: https://google.github.io/styleguide/javaguide.html
[py]: https://google.github.io/styleguide/pyguide.html
[sh]: https://google.github.io/styleguide/shell.xml
[htmlcss]: https://google.github.io/styleguide/htmlcssguide.xml
[js]: https://google.github.io/styleguide/javascriptguide.xml
[vim]: https://google.github.io/styleguide/vimscriptguide.xml
[xml]: https://google.github.io/styleguide/xmlstyle.html
[csharp]: https://msdn.microsoft.com/en-us/library/ff926074.aspx
[php]: http://www.php-fig.org/psr/psr-2/
[phpstorm]:https://www.jetbrains.com/phpstorm/
[pycharm]:https://www.jetbrains.com/pycharm/
[webstorm]:https://www.jetbrains.com/webstorm/
[idea]: https://www.jetbrains.com/idea/
[visualstudio]:https://www.visualstudio.com/products/vs-2015-product-editions
[atom]: https://atom.io/
[vcode]: https://code.visualstudio.com/
[sublime]: https://www.sublimetext.com/
